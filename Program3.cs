﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

//Threading with main thread and 3 custom thread.
//Three make pizza and another wait for it.
//Use: Thread, thread.Join()
namespace CSharpMultithreading {
    class MyThread {
        public int currentCount;
        public int totalCount;
        public Thread thread;

        public MyThread(string name, int totalCount_) {
            currentCount = 0;
            totalCount = totalCount_;
            thread = new Thread(Run);
            thread.Name = name;
            thread.Start();
        }

        private void Run() {
            Console.WriteLine(thread.Name + " started.");

            do {
                Thread.Sleep(500);
                currentCount++;
                Console.WriteLine("In thread " + thread.Name + ", create pizza num = " + currentCount);
            } while (currentCount < totalCount);

            Console.WriteLine(thread.Name + " finished.");
        }
    }
    
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("Main thread started.");

            int numPizzasCustomerNeed = 30;
            int numTakenPizzas = 0;

            Console.WriteLine("Create " + numPizzasCustomerNeed + " pizzas to me.");

            MyThread pizzaMaker1 = new MyThread("PizzaMakerThread_1", numPizzasCustomerNeed / 3);
            MyThread pizzaMaker2 = new MyThread("PizzaMakerThread_2", numPizzasCustomerNeed / 3);
            MyThread pizzaMaker3 = new MyThread("PizzaMakerThread_3", numPizzasCustomerNeed / 3);

            pizzaMaker1.thread.Join();
            numTakenPizzas += pizzaMaker1.currentCount;
            Console.WriteLine("Customer has " + numTakenPizzas + " pizzas.");

            pizzaMaker2.thread.Join();
            numTakenPizzas += pizzaMaker2.currentCount;
            Console.WriteLine("Customer has " + numTakenPizzas + " pizzas.");

            pizzaMaker3.thread.Join();
            numTakenPizzas += pizzaMaker3.currentCount;
            Console.WriteLine("Customer has " + numTakenPizzas + " pizzas.");

            Console.WriteLine("Thanks for " + numTakenPizzas + " pizzas!");

            Console.WriteLine("Main thread finished.");
        }
    }
}