﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

//Locking
//Use: Thread, lock
namespace CSharpMultithreading {
    class SumArray {
        int sum;
        object lockOn = new object();

        public int SumIt(int[] numbers) {
            lock (lockOn) {
                sum = 0;

                for (int i = 0; i < numbers.Length; i++) {
                    sum += numbers[i];

                    Console.WriteLine("Current sum for thread " + Thread.CurrentThread.Name +
                        " = " + sum);

                    Thread.Sleep(100);
                }

                return sum;
            }
        }
    }

    class MyThread {
        public Thread thread;
        private int answer;

        static SumArray sa = new SumArray();

        public MyThread(string name, int[] numbers) {
            thread = new Thread(Run);
            thread.Name = name;
            thread.Start(numbers);
        }

        private void Run(object numbersObj) {
            Console.WriteLine(thread.Name + " started.");

            sa.SumIt(numbersObj as int[]);

            Console.WriteLine("Sum for the thread " + thread.Name + " = " + answer);
            Console.WriteLine(thread.Name + " finished.");
        }
    }
    
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("Main thread started.");

            int[] numbers = { 1, 2, 3, 4, 5};

            MyThread mt1 = new MyThread("Thread_1", numbers);
            MyThread mt2 = new MyThread("Thread_2", numbers);

            mt1.thread.Join();
            mt2.thread.Join();

            Console.WriteLine("Main thread finished.");
        }
    }
}