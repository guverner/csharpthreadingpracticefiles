﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

//Locking
//Use: Thread, lock
namespace CSharpMultithreading {
    class Pizzas {
        private object locker = new object();
        private int numMakedPizzas;

        public void Make() {
            lock (locker) {
                numMakedPizzas++;
                Console.WriteLine("Pizza " + numMakedPizzas + " -> " + Thread.CurrentThread.Name);
            }

            Console.WriteLine("REST -> " + Thread.CurrentThread.Name);
            Thread.Sleep(50);
        }
    }

    class MyThread {
        public Thread thread;
        public static Pizzas pizzas = new Pizzas();
        private int numPizzasCreate;

        public MyThread(string name, int numPizzasCreate_) {
            numPizzasCreate = numPizzasCreate_;
            thread = new Thread(Run);
            thread.Name = name;
            thread.Start();
        }

        private void Run() {
            Console.WriteLine(thread.Name + " START");

            for (int i = 0; i < numPizzasCreate; i++) {
                pizzas.Make();
            }
            Console.WriteLine(thread.Name + " FINISH");
        }
    }
    
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("Main() START");

            const int NUM_PIZZAS_TOTAL = 10;

            MyThread pizzaMaker1 = new MyThread("Thread_1", NUM_PIZZAS_TOTAL / 2);
            MyThread pizzaMaker2 = new MyThread("Thread_2", NUM_PIZZAS_TOTAL / 2);

            pizzaMaker1.thread.Join();
            pizzaMaker2.thread.Join();

            Console.WriteLine("Main() FINISH");
        }
    }
}