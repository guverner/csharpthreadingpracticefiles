﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

//Locking
//Use: Task
namespace CSharpMultithreading {
    class Program {
        private static int i = 0;

        static void Main(string[] args) {
            Console.WriteLine("START -> Main()");

            Task task = new Task(OutputNumbers);
            task.Start();

            while (i < 10) {
                Thread.Sleep(500);
            }

            Console.WriteLine("FINISH -> Main()");
        }

        public static void OutputNumbers() {
            for (; i < 10; i++) {
                Thread.Sleep(500);
                Console.WriteLine("Number = " + i);
            }
        }
    }
}