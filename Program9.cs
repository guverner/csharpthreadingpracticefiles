﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

//Locking
//Use: Thread, lock, Monitor.Pulse, Monitor.Wait
namespace CSharpMultithreading {
    class MyData {
        private object locker = new object();

        public void Tic() {
            lock (locker) {
                Console.WriteLine("tic");
                Monitor.Pulse(locker);
                Monitor.Wait(locker);
            }
        }

        public void Tac() {
            lock (locker) {
                Console.WriteLine("tac");
                Monitor.Pulse(locker);
                Monitor.Wait(locker);
            }
        }
    }

    class MyThread {
        private Thread thread;
        private MyData data;
        private bool isTic;

        public MyThread(string name_, MyData data_, bool isTic_) {
            data = data_;
            isTic = isTic_;

            Console.WriteLine("INIT " + name_);
            thread = new Thread(Run);
            thread.Name = name_;
            thread.Start();
        }

        private void Run() {
            Console.WriteLine("START -> " + thread.Name);

            for (int i = 0; i < 10; i++) {
                if (isTic)
                    data.Tic();
                else
                    data.Tac();
            }

            Console.WriteLine("FINISH -> " + thread.Name);
        }

        public void MakeWaitCallerThread() {
            thread.Join();
        }
    }

    class Program {
        static void Main(string[] args) {
            Console.WriteLine("START -> Main()");

            MyData data = new MyData();

            MyThread thread1 = new MyThread("thread_tic", data, true);
            MyThread thread2 = new MyThread("thread_tac", data, false);

            thread1.MakeWaitCallerThread();
            thread2.MakeWaitCallerThread();

            Console.WriteLine("FINISH -> Main()");
        }
    }
}