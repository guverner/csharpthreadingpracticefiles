﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

//Locking
//Use: Thread, lock
namespace CSharpMultithreading {
    class Pizzas {
        private object locker = new object();
        private object locker2 = new object();
        private int numNeedMakePizzas;
        private int numMakedPizzas;
        private int numMakingPizzas;
        
        public Pizzas(int numNeedMakePizzas_) {
            numNeedMakePizzas = numNeedMakePizzas_;
        }

        public void MakeLoop() {
            while (IsNeedMakeAnotherOneMake()) {
                //Console.WriteLine("IN LOCKER IsNeedMakeAnotherOneMake() -> " + Thread.CurrentThread.Name);
                Make();
            }
        }

        public void Make() {
            lock (locker2) {
                //Console.WriteLine("IN LOCKER Make() -> " + Thread.CurrentThread.Name);
                numMakedPizzas++;
                numMakingPizzas--;
                Console.WriteLine("Pizza " + numMakedPizzas + " -> " + Thread.CurrentThread.Name);
            }

            //Console.WriteLine("OUT LOCKER Make() -> " + Thread.CurrentThread.Name);
            //Console.WriteLine("REST -> " + Thread.CurrentThread.Name);
            Thread.Sleep(100);
        }

        private bool IsNeedMakeAnotherOneMake() {
            lock (locker) {
                //Console.WriteLine("IN LOCKER IsNeedMakeAnotherOneMake() -> " + Thread.CurrentThread.Name);
                if (numMakedPizzas + numMakingPizzas < numNeedMakePizzas) {
                    //Console.WriteLine("A = " + (numMakedPizzas + numMakingPizzas) + 
                    //    " -> " + Thread.CurrentThread.Name);
                    numMakingPizzas++;
                    return true;
                }
                else {
                    Console.WriteLine("ENOUGHT numMakingPizzas = " + numMakingPizzas + " -> " + Thread.CurrentThread.Name);
                    return false;
                }
            }
        } 
    }

    class MyThread {
        public Thread thread;
        public static Pizzas pizzas = new Pizzas(100);

        public MyThread(string name) {
            thread = new Thread(Run);
            thread.Name = name;
            thread.Start();
        }

        private void Run() {
            Console.WriteLine(thread.Name + " START");
            pizzas.MakeLoop();
            Console.WriteLine(thread.Name + " FINISH");
        }
    }
    
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("Main() START");

            DateTime startedTime = DateTime.Now;

            const int NUM_THREADS = 10;

            MyThread[] threads = new MyThread[NUM_THREADS];

            for (int i = 0; i < NUM_THREADS; i++) {
                threads[i] = new MyThread("Thread_" + (i + 1));
            }

            for (int i = 0; i < NUM_THREADS; i++) {
                threads[i].thread.Join();
            }

            TimeSpan timePassed = DateTime.Now - startedTime;

            Console.WriteLine("Main() FINISH timePassed = " + timePassed.TotalMilliseconds);
        }
    }
}