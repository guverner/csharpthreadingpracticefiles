﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

//Threading with main thread and 1 custom thread.
//One make pizza and another wait for it.
//Use: Thread, Thread.Sleep()
namespace CSharpMultithreading {
    class MyThread {
        public int currentCount;
        public int totalCount;
        public Thread thread;

        public MyThread(string name, int totalCount_) {
            currentCount = 0;
            totalCount = totalCount_;
            thread = new Thread(Run);
            thread.Name = name;

            thread.Start();
        }

        private void Run() {
            Console.WriteLine(thread.Name + " started.");

            do {
                Thread.Sleep(500);
                currentCount++;
                Console.WriteLine("In thread " + thread.Name + ", create pizza num = " + currentCount);
            } while (currentCount < totalCount);

            Console.WriteLine(thread.Name + " finished.");
        }
    }
    
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("Main thread started.");

            int numTakenPizza = 0;

            MyThread myThread = new MyThread("Thread_1", 10);

            do {
                Console.Write(".");
                Thread.Sleep(100);

                if(myThread.currentCount > 0 && myThread.currentCount == numTakenPizza + 1) {
                    numTakenPizza++;
                    Console.WriteLine("Customer take piza num = " + numTakenPizza + ".");
                }   
            } while (myThread.currentCount != myThread.totalCount);

            Console.WriteLine("Main thread finished.");
        }
    }
}
