﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

//Threading with main thread and 1 custom thread.
//One make pizza and another wait for it.
//Use: Thread, Thread.Sleep(), thread.IsAlive
namespace CSharpMultithreading {
    class MyThread {
        public int currentCount;
        public int totalCount;
        public Thread thread;

        public MyThread(string name, int totalCount_) {
            currentCount = 0;
            totalCount = totalCount_;
            thread = new Thread(Run);
            thread.Name = name;

            thread.Start();
        }

        private void Run() {
            Console.WriteLine(thread.Name + " started.");

            do {
                Thread.Sleep(500);
                currentCount++;
                Console.WriteLine("In thread " + thread.Name + ", create pizza num = " + currentCount);
            } while (currentCount < totalCount);

            Console.WriteLine(thread.Name + " finished.");
        }
    }
    
    class Program {
        static void Main(string[] args) {
            Console.WriteLine("Main thread started.");

            int numTakenPizza = 0;
            int numPizzasCustomerNeed = 10;

            MyThread pizzaMaker1 = new MyThread("PizzaMakerThread_1", numPizzasCustomerNeed);

            do {
                Console.Write(".");
                Thread.Sleep(100);

                if ((pizzaMaker1.currentCount > 0) &&
                    pizzaMaker1.currentCount == numTakenPizza + 1) {
                    numTakenPizza++;
                    Console.WriteLine("Customer take piza num = " + numTakenPizza + ".");
                }
            } while (pizzaMaker1.thread.IsAlive);

            Console.WriteLine("Main thread finished.");
        }
    }
}
