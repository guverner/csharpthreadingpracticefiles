﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

//Locking
//Use: Thread, lock
namespace CSharpMultithreading {
    class Pizzas {
        private int numNeedMakePizzas;
        private int numPizzasCanTake;
        private int numMakingPizzas;

        public void AddNeededPizzas(int numPizzasNeed_) {
            numNeedMakePizzas += numPizzasNeed_;
            Console.WriteLine("numNeedMakePizzas = " + numNeedMakePizzas + " -> " + Thread.CurrentThread.Name);
        }

        private object locker = new object();
        public bool Take() {
            lock (locker) {
                if (numPizzasCanTake > 0) {
                    numPizzasCanTake--;
                    return true;
                }
                else return false;
            }
        }

        private object locker3 = new object();
        public void Make() {
            lock (locker3) {
                //Console.WriteLine("IN LOCKER Make() -> " + Thread.CurrentThread.Name);
                numPizzasCanTake++;
                numMakingPizzas--;
                numNeedMakePizzas--;
                Console.WriteLine("PIZZA CAN TAKE = " + numPizzasCanTake + " -> " + Thread.CurrentThread.Name);
            }

            //Console.WriteLine("OUT LOCKER Make() -> " + Thread.CurrentThread.Name);
            //Console.WriteLine("REST -> " + Thread.CurrentThread.Name);
            Thread.Sleep(50);
        }

        private object locker2 = new object();
        public bool IsNeedMakeAnotherOne() {
            lock (locker2) {
                //Console.WriteLine("IN LOCKER IsNeedMakeAnotherOneMake() -> " + Thread.CurrentThread.Name);
                if (numMakingPizzas < numNeedMakePizzas) {
                    //Console.WriteLine("A = " + (numMakedPizzas + numMakingPizzas) + 
                    //    " -> " + Thread.CurrentThread.Name);
                    numMakingPizzas++;
                    return true;
                }
                else {
                    //Console.WriteLine("ENOUGHT numMakingPizzas = " + numMakingPizzas + " -> " + Thread.CurrentThread.Name);
                    return false;
                }
            }
        }
    }

    class PizzaMaker {
        public Thread thread;
        private Pizzas pizzas;
        private Restaraunt restaraunt;

        public PizzaMaker(string name, Pizzas pizzas_, Restaraunt restaraunt_) {
            pizzas = pizzas_;
            restaraunt = restaraunt_;
            thread = new Thread(Run);
            thread.Name = name;
            thread.Start();
        }

        private void Run() {
            Console.WriteLine("START -> " + thread.Name);

            int numMakedPizzas = 0;

            while (restaraunt.IsNeedPizaMaker()) {
                if (pizzas.IsNeedMakeAnotherOne()) {
                    pizzas.Make();
                    numMakedPizzas++;
                }
                else {
                    Thread.Sleep(10);
                }
            }

            Console.WriteLine("MAKED = " + numMakedPizzas + " -> " + thread.Name);
            Console.WriteLine("FINISH -> " + thread.Name);
        }
    }

    class Customer {
        public Thread thread;
        private int numPizzasNeed;
        private int numPizzasTaken;
        private Pizzas pizzas;
        private Restaraunt restaraunt;

        public Customer(string name, int numPizzasNeed_, Pizzas pizzas_, Restaraunt restaraunt_) {
            pizzas = pizzas_;
            restaraunt = restaraunt_;
            numPizzasNeed = numPizzasNeed_;
            thread = new Thread(Run);
            thread.Name = name;
            thread.Start();
        }

        private void Run() {
            Console.WriteLine("START -> " + thread.Name);

            restaraunt.TakeOrder(numPizzasNeed);

            while (numPizzasTaken < numPizzasNeed) {
                if (pizzas.Take()) {
                    numPizzasTaken++;
                    Console.WriteLine("TAKE have = " + numPizzasTaken + " -> " + thread.Name);
                }
                else Wait();
            }

            Console.WriteLine("numPizzasTaken = " + numPizzasTaken + " -> " + thread.Name);

            lock(restaraunt) restaraunt.RemoveCustomer();

            Console.WriteLine("FINISH -> " + thread.Name);
        }

        private void Wait() {
            Thread.Sleep(10);
        }

        public void Pause() {
            if(thread.IsAlive)
                thread.Suspend();
        }
    }

    class Restaraunt {
        private PizzaMaker[] pizzaMakers;
        private Pizzas pizzas;
        private int numServedCustomers;

        public Restaraunt(int numPizzaMakers, Pizzas pizzas_) {
            pizzas = pizzas_;
            pizzaMakers = new PizzaMaker[numPizzaMakers];

            for (int i = 0; i < numPizzaMakers; i++) {
                pizzaMakers[i] = new PizzaMaker("pizzaMaker_" + (i + 1), pizzas, this);
            }
        }

        public void TakeOrder(int numPizzasNeed) {
            lock (pizzas) {
                pizzas.AddNeededPizzas(numPizzasNeed);
            }
        }

        public void WaitForAll() {
            for (int i = 0; i < pizzaMakers.Length; i++) {
                pizzaMakers[i].thread.Join();
            }
        }

        public void Pause() {
            for (int i = 0; i < pizzaMakers.Length; i++) {
                if(pizzaMakers[i].thread.IsAlive)
                    pizzaMakers[i].thread.Suspend();
            }
        }

        public void RemoveCustomer() {
            numServedCustomers++;
        }

        private object locker = new object();
        public bool IsNeedPizaMaker() {
            lock (locker) {
                if(numServedCustomers < 3) {
                    return true;
                }
                return false;
            }
        }
    }

    class Program {
        static void Main(string[] args) {
            Console.WriteLine("START -> Main()");

            DateTime startedTime = DateTime.Now;

            Pizzas pizzas = new Pizzas();
            Restaraunt restaraunt = new Restaraunt(20, pizzas);

            Customer customer1 = new Customer("customer_1", 3, pizzas, restaraunt);
            Customer customer2 = new Customer("customer_2", 10, pizzas, restaraunt);
            Customer customer3 = new Customer("customer_3", 8, pizzas, restaraunt);

            Thread thread = new Thread(() => {
                while (true) {
                    if (Console.ReadKey().Key == ConsoleKey.Q) {
                        Console.WriteLine("Pressed Q!!!");
                        restaraunt.Pause();
                        customer1.Pause();
                        customer2.Pause();
                        customer3.Pause();
                        Console.ReadKey();
                    }
                }
            });
            thread.IsBackground = true;
            thread.Start();

            customer1.thread.Join();
            customer2.thread.Join();
            customer3.thread.Join();
            restaraunt.WaitForAll();

            TimeSpan timePassed = DateTime.Now - startedTime;

            Console.WriteLine("FINISH -> Main()");
            Console.WriteLine("timePassed = " + timePassed.TotalMilliseconds);
        }
    }
}